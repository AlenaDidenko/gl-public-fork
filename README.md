# 👋 This repo is managed by Versions #

**[🚀 Open this repo in the Versions desktop app](https://app-versions.sympli.io/auth/redirect?provider=gitlab&id=5df338c840160475f88633e1).** The Versions desktop app is a full-featured DesignOps and collaboration tool that provides advanced version control features like visual diff, merge and conflict resolution. 

-- OR --

**[Open this repo in the Versions web app](https://app-versions.sympli.io/project/5df338c840160475f88633e1?from=gitlab).** The Versions web app lets you monitor activity, inspect changes and download mockups. 

### Don't have the Versions desktop app? ###

No problem! [Download it from our website](http://versions.sympli.io).

### Have questions? ###

Shoot us an email at [versions@sympli.io](mailto:versions@sympli.io) 
